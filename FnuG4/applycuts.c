EdbEDA *eda;
class Particle: public TObject{
	public:
	
	int id;
	int index;
	int idParent;
	EdbTrackP *t;
	Particle *parent;
	TObjArray *daughters;
	
	void Print();
};

void Particle::Print(){
	printf("%6d %5d %4d -> %4d \n", id, ev->pdgid[index], t->GetSegmentFirst()->PID(), t->GetSegmentLast()->PID());
}

FnuG4 *ev;

TObjArray *particles = new TObjArray;
TObjArray *reachingParticles = new TObjArray;

Particle* FindParticle(int id){
	
	int nparticles = particles->GetEntriesFast();
	for(int i=0; i<nparticles; i++){
		Particle *p = (Particle *) particles->At(i);
		if(p->id==id) return p;
	}
	return NULL;
}


Particle* AddParticle(int idx){
	
	Particle *p = new Particle;
	p->id = ev->id[idx];
	p->index = idx;
	p->idParent = ev->idParent[idx];
	p->t = new EdbTrackP;
	p->t->SetP( sqrt( ev->px[idx]*ev->px[idx]+ev->py[idx]*ev->py[idx]+ev->pz[idx]*ev->pz[idx] ) );
	particles->Add(p);
	
	return p;
}





	
	
	
	




//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%





EdbPVRec *psuedRec(FnuG4 *ev){
	
	EdbPVRec *pvr = new EdbPVRec;
	int nhits = ev->nhits;
	TDatabasePDG db;
	
	for(int i=0; i<nhits; i++){
//		if(i<100) printf("%5d %2d %7.1f %7.1f %7.1f %7.2f %7.2f %7.2f %7.2f\n", 
//		    ev->pdgid[i], ev->charge[i], ev->x[i], ev->y[i], ev->z[i], ev->px[i], ev->py[i], ev->pz[i], ev->e2[i]);
		
		if(ev->charge[i]==0) continue;
		if(ev->izsub[i]==1) continue;
		double slope = sqrt( ev->px[i]*ev->px[i]/ev->pz[i]/ev->pz[i] + ev->py[i]*ev->py[i]/ev->pz[i]/ev->pz[i]);
		if(slope>0.1) continue;

		EdbSegP *s = new EdbSegP(i, ev->x[i]*1e3, ev->y[i]*1e3, ev->px[i]/ev->pz[i], ev->py[i]/ev->pz[i], ev->edep[i]*200+16, ev->pdgid[i]);
		s->SetZ(ev->z[i]*1e3);
		s->SetPID(ev->iz[i]);
		s->SetPlate(ev->iz[i]);
		s->SetP( sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i]+ev->pz[i]*ev->pz[i]));
		ntracks_z->Fill(ev->z[i]);
		cout << "Added track segment with z = " << ev->z[i] << endl;
		//if(sqrt(s->TX()**2+s->TY()**2)>1) continue;
		
		EdbTrackP *t = new EdbTrackP(s);
		
		pvr->AddTrack(t);
		
		
	}
	
	return pvr;
	
}

void display(EdbPVRec *pvr){
	eda = new EdbEDA(pvr,0);
	eda->GetTrackSet("TS")->SetExtendMode(1);
	eda->Run();
}
void display(){
	printf("Open Xin's file\n");
	ev = new FnuG4();

	int iev = 0;
	ev->GetEntry(iev);

	EdbPVRec *pvr = psuedRec(ev);
	display(pvr);
}









void applycuts(){
	
	printf("Open Xin's file\n");
	
	ev = new FnuG4();
	
	TFile f("000_FASERnu_cuts.root","recreate");
	TNtuple *z = new TNtuple("ntracks_z","","z:iz");
	TTree *params = new TTree("params","params");
	TH1D *ntrk = new TH1D("ntrk","",100,-700,200);
	TH1D *iz = new TH1D("iz","",100,0,1000);
	TH1D *izfine = new TH1D("iz","",1000,0,1000);
	TH1D *ntrk_pileup = new TH1D("ntrk_pileup","",100,-700,200);
	TH2D *ntrk_iz = new TH2D("ntrk_iz","",50,0,1000,50,10,40000);
	TH1D *iz_pileup = new TH1D("iz_pileup","iz vs. E",1000,0,1000);
	hprof = new TProfile("hprof","Profile of ntracks vs. depth",100,0,1000,0,40000);
	nprof = new TProfile("nprof","Profile of ntracks",100,0,1000,0,40000);
	nmaxprof = new TProfile("nmaxprof","Profile of ntracks",100,0,1000,0,40000);
	izprof = new TProfile("izprof","Profile of ntracks",100,0,1000,0,40000);
	
	TNtuple *nt = new TNtuple("nt","","iev:CC:Enu:x:y:z:ntracks");
	TNtuple *ntReso = new TNtuple("ntReso","","iev:CC:pdg:p:dx0:dy0:dx1:dy1:dx2:dy2:dx3:dy3");
	TNtuple *ntResoSmeared = new TNtuple("ntResoSmeared","","iev:CC:pdg:p:dx0:dy0:dx1:dy1:dx2:dy2:dx3:dy3");
                std::vector<float>* ntotal;
                std::vector<float>* nmax;
                std::vector<float>* izmax;
		params->Branch("ntotal",&ntotal);
		params->Branch("nmax",&nmax);
		params->Branch("izmax",&izmax);
		ntotal->clear();
		nmax->clear();
		izmax->clear();
		double avg = 0;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for(int iev=3202; iev<3203	; iev++){ // 3202 is electron
		if(iev==12219||iev==13771||iev==21440)continue;

		ev->GetEntry(iev);
		int nhits = ev->nhits;
		
		printf("\n\n\n\n [[[[[ iev %d ]]]]] nhits=%d \n", iev, nhits);
		
//		ev->Print();
//		trackcount=0;
		
		// fake reconstruction.
		// track followdown in this func.
		ntrk->Reset();
//		ntrk_pileup->Reset(); // Commented out to do pileup
//		iz_pileup->Reset(); // Commented out to do pileup
		int count = 0;
		int nmaxvalue = 0;
		int izmaxvalue = 0;
		for(int i=0; i<nhits; i++){
			if(ev->izsub[i]!=0) continue; // There are two hits for 1 emulsion film. Ignore one of them.
			if(ev->charge[i]==0) continue; // ignore neutral particles (photons, neutral hadrons) at truth level
//==========ADDITIONAL CUTS===========//
//			printf("R = %d\n",ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i]);
//			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])>100) continue; // ignore everything outside of 100 um cylinder (optimizing)
//			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])>0.1*abs(ev->z[i]+700.)) continue; // ignore everything outside of 0.001 rad cone (optimizing)
//			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])<0.0001*abs(ev->z[i]+700.)) continue; // muon hits are close to axis, ignore them (optimizing)
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i])/ev->pz[i]>0.05) continue; // Keep only hits with track slope < 50 mrad
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i])/ev->pz[i]<0.0003) continue; // Muon hits have almost normal tracks, ignore (optimizing)
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i]+ev->pz[i]*ev->pz[i])<1000) continue; // Keep only hits with energy > 100 MeV
			//cout << "x = " << ev->x[i] << " y = " << ev->y[i] << endl;
			z->Fill(ev->z[i],ev->iz[i]);
			ntrk->Fill(ev->z[i]);
			ntrk_pileup->Fill(ev->z[i]);
			iz->Fill(ev->iz[i]);
			iz_pileup->Fill(ev->iz[i]);
			count++;
		}
		avg = avg + count/10.;
		cout << "avg = " << avg << endl;
		cout << "\ncount = " << count << endl;
		izmaxvalue = ntrk->GetMaximumBin();
		cout << "izmax = " << izmaxvalue << endl;
		nmaxvalue = ntrk->GetMaximum();
		cout << "nmax = " << nmaxvalue << endl;
		nprof->Fill(1,count);
		nmaxprof->Fill(1,nmaxvalue);
		izprof->Fill(1,izmaxvalue);
		ntotal->push_back(count);
		nmax->push_back(nmaxvalue);
		izmax->push_back(izmaxvalue);
		for(int i=0;i<100;i++){
			double zVal = ntrk->GetXaxis()->GetBinCenter(i);
			double izVal = iz->GetXaxis()->GetBinCenter(i);
			int nVal = ntrk->GetBinContent(i);
//			cout << "center = " << zVal << endl;
//			cout << "bin content = " << nVal << endl;
//			ntrk_z->Fill(zVal,nVal);
			ntrk_iz->Fill(izVal,nVal);
			hprof->Fill(izVal,nVal);
		}
	}
	params->Fill();
	params->Write();
	hprof->Write();
	nprof->Write();
	nmaxprof->Write();
	izprof->Write();
	ntrk->Write();
	iz->Write();
	ntrk_pileup->Write();
	iz_pileup->Write();
	ntrk_iz->Write();
	ntracks_z->Write();
	nt->Write();
	f.Close();
//	cout << "HITS = " << avg << endl;
//     EdbPVRec *pvr = psuedRec(ev); 	display(pvr);
//	TFile f("000_FASERnu_cuts.root");
//	TProfile *p = f.Get("nprof");
//	cout << p->GetMaximum() << endl;
	//cout << "HITS = " << t->GetBranch("nprof")->GetMaximum() << endl;
}

