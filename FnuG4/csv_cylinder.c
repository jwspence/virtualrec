void csv_cylinder(){
	
	ev = new FnuG4();
	
	TFile f("000_FASERnu_cuts.root","recreate");
	TNtuple *z = new TNtuple("ntracks_z","","z:iz");
	TTree *params = new TTree("params","params");
	TH1D *ntrk = new TH1D("ntrk","",100,-700,200);
	TH1D *iz = new TH1D("iz","",1000,0,1000);
	TH1D *ntrk_pileup = new TH1D("ntrk_pileup","",100,-700,200);
	TH2D *ntrk_iz = new TH2D("ntrk_iz","",50,0,1000,50,10,40000);
	TH1D *iz_pileup = new TH1D("iz_pileup","iz vs. E",1000,0,1000);
	hprof = new TProfile("hprof","Profile of ntracks vs. depth",100,0,1000,0,40000);
	nprof = new TProfile("nprof","Profile of ntracks",100,0,1000,0,40000);
	nmaxprof = new TProfile("nmaxprof","Profile of ntracks",100,0,1000,0,40000);
	izprof = new TProfile("izprof","Profile of ntracks",100,0,1000,0,40000);
	
	TNtuple *nt = new TNtuple("nt","","iev:CC:Enu:x:y:z:ntracks");
	TNtuple *ntReso = new TNtuple("ntReso","","iev:CC:pdg:p:dx0:dy0:dx1:dy1:dx2:dy2:dx3:dy3");
	TNtuple *ntResoSmeared = new TNtuple("ntResoSmeared","","iev:CC:pdg:p:dx0:dy0:dx1:dy1:dx2:dy2:dx3:dy3");
                std::vector<float>* ntotal;
                std::vector<float>* nmax;
                std::vector<float>* izmax;
		params->Branch("ntotal",&ntotal);
		params->Branch("nmax",&nmax);
		params->Branch("izmax",&izmax);
		ntotal->clear();
		nmax->clear();
		izmax->clear();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	for(int cut1=1;cut1<20;cut1++){
		cout << "cut1 = " << cut1 << endl;
	for(int iev=0; iev<1000	; iev++){ // 3202 is electron
		if(iev==11929||iev==13543||iev==21425)continue;
		ev->GetEntry(iev);
		int nhits = ev->nhits;
		iz->Reset();
		int count = 0;
		int nmaxvalue = 0;
		int izmaxvalue = 0;
		for(int i=0; i<nhits; i++){
			if(ev->izsub[i]!=0) continue; // There are two hits for 1 emulsion film. Ignore one of them.
			if(ev->charge[i]==0) continue; // ignore neutral particles (photons, neutral hadrons) at truth level
//==========ADDITIONAL CUTS===========//
//			printf("R = %d\n",ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i]);
			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])>cut1/100.) continue; // ignore everything outside of 100 um cylinder (optimizing)
//			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])>0.1*abs(ev->z[i]+700.)) continue; // ignore everything outside of 0.001 rad cone (optimizing)
//			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])<0.0001*abs(ev->z[i]+700.)) continue; // muon hits are close to axis, ignore them (optimizing)
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i])/ev->pz[i]>0.05) continue; // Keep only hits with track slope < 50 mrad
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i])/ev->pz[i]<0.0003) continue; // Muon hits have almost normal tracks, ignore (optimizing)
//			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i]+ev->pz[i]*ev->pz[i])<1000) continue; // Keep only hits with energy > 100 MeV
			//cout << "x = " << ev->x[i] << " y = " << ev->y[i] << endl;
			z->Fill(ev->z[i],ev->iz[i]);
			ntrk->Fill(ev->z[i]);
			ntrk_pileup->Fill(ev->z[i]);
			iz->Fill(ev->iz[i]);
			iz_pileup->Fill(ev->iz[i]);
			count++;
		}
		cout << iev << " ";
		cout << count << " ";
		nmaxvalue = iz->GetMaximum();
		cout << nmaxvalue << " ";
		izmaxvalue = iz->GetMaximumBin();
		cout << izmaxvalue << " ";
		for(int j=0;j<1000;j++){
			cout << iz->GetBinContent(j) << " ";
		}
		cout << endl;
		nprof->Fill(1,count);
		nmaxprof->Fill(1,nmaxvalue);
		izprof->Fill(1,izmaxvalue);
		ntotal->push_back(count);
		nmax->push_back(nmaxvalue);
		izmax->push_back(izmaxvalue);
		for(int i=0;i<100;i++){
			double zVal = ntrk->GetXaxis()->GetBinCenter(i);
			double izVal = iz->GetXaxis()->GetBinCenter(i);
			int nVal = ntrk->GetBinContent(i);
//			cout << "center = " << zVal << endl;
//			cout << "bin content = " << nVal << endl;
//			ntrk_z->Fill(zVal,nVal);
			ntrk_iz->Fill(izVal,nVal);
			hprof->Fill(izVal,nVal);
		}
	}
}
	params->Fill();
	params->Write();
	hprof->Write();
	nprof->Write();
	nmaxprof->Write();
	izprof->Write();
	ntrk->Write();
	iz->Write();
	ntrk_pileup->Write();
	iz_pileup->Write();
	ntrk_iz->Write();
	ntracks_z->Write();
	nt->Write();
	f.Close();
//	cout << "HITS = " << avg << endl;
//     EdbPVRec *pvr = psuedRec(ev); 	display(pvr);
//	TFile f("000_FASERnu_cuts.root");
//	TProfile *p = f.Get("nprof");
//	cout << p->GetMaximum() << endl;
	//cout << "HITS = " << t->GetBranch("nprof")->GetMaximum() << endl;
	cout << "FINISHED" << endl;
}
