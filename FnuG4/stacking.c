EdbEDA *eda;
class Particle: public TObject{
	public:
	
	int id;
	int index;
	int idParent;
	EdbTrackP *t;
	Particle *parent;
	TObjArray *daughters;
	
	void Print();
};

void Particle::Print(){
	printf("%6d %5d %4d -> %4d \n", id, ev->pdgid[index], t->GetSegmentFirst()->PID(), t->GetSegmentLast()->PID());
}

FnuG4 *ev;

TObjArray *particles = new TObjArray;
TObjArray *reachingParticles = new TObjArray;

Particle* FindParticle(int id){
	
	int nparticles = particles->GetEntriesFast();
	for(int i=0; i<nparticles; i++){
		Particle *p = (Particle *) particles->At(i);
		if(p->id==id) return p;
	}
	return NULL;
}


Particle* AddParticle(int idx){
	
	Particle *p = new Particle;
	p->id = ev->id[idx];
	p->index = idx;
	p->idParent = ev->idParent[idx];
	p->t = new EdbTrackP;
	p->t->SetP( sqrt( ev->px[idx]*ev->px[idx]+ev->py[idx]*ev->py[idx]+ev->pz[idx]*ev->pz[idx] ) );
	particles->Add(p);
	
	return p;
}

///  SCT related
void GetSctStrip(float x, float y, int *iu, int *iv){
	
	float qu = 0.02; // 20 mrad tilted
	float qv = -0.02; // -20 mrad tilted
	float xu = (120-x)*tan(qu) + y; // absolute position
	float xv = (120-x)*tan(qv) + y;
	
	*iu = (int) xu*cos(qu)/80.e-3;
	*iv = (int) xv*cos(qv)/80.e-3;
	
	printf("xu = %.1f, iu = %d\n", xu, *iu);
	printf("xv = %.1f, iv = %d\n", xv, *iv);

}




int trackcount=0;

void trackfollow(Particle *pparent, int depth){
	// Follow-down tracks. This function is nested.
	// If it reach the Z coordinate bigger than 603200, say "It reach the end of FASERnu".
	// Add it in the global TObjArray *reachingParticles.
	
	if(ev->e1[pparent->index]<1) return;
	printf("stop z = %f\n", pparent->t->GetSegmentLast()->Z());
	if( pparent->t->GetSegmentLast()->Z() >603200) {
		trackcount++;
		printf("Reached to the end of FASERnu\n");
		reachingParticles->Add(pparent);
//		pparent->t->PrintNice();
	}
	
	for(int i=0; i<particles->GetEntriesFast(); i++){
		Particle *p = (Particle *) particles->At(i);
		if(p->idParent == pparent->id){
			// this is the daughter of the parent
			for(int j=0; j<depth+1; j++) printf(" ");
			printf("%dry daughter %5d ", depth, p->id);
			if(ev->charge[p->index]==0) printf("neutral, stop.\n");
			else if(p->t->P()<1000) printf("Low energy, stop.\n");
			else {
				printf("charged, follow down.\n");
				trackfollow(p, depth+1);
			}
		}
	}
}	
	
	
	
	




//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void recTree(){
	
	cout << "calling recTree()..." << endl;
	int nhits = ev->nhits;
	particles->Clear(); // clear previous event.
	reachingParticles->Clear();
	
	int nchargedhits = 0;
	for(int i=0; i<nhits; i++){
		if(ev->izsub[i]!=0) continue; // There are two hits for 1 emulsion film. Ignore one of them.
		if(ev->pdgid[i]!=-11 && ev->pdgid[i]!=11) continue; // ignore everything apart from electrons. 
//		cout << "PDGID = " << ev->pdgid[i] << endl;
//		if(ev->e1[i]<100) continue;
		if(fabs(ev->charge[i])==0) continue;
		if(fabs(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i])/ev->pz[i])>0.1) continue;
		if(fabs(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i]+ev->pz[i]*ev->pz[i])<10)) continue;
		if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i]+ev->z[i]*ev->z[i])>200) continue;
//		cout << "px = " << ev->px[i] << endl;
//		cout << "pz = " << ev->pz[i] << endl;
//		cout << "px/pz = " << ev->px[i]/ev->pz[i] << endl;
		
//		printf("%6d %6d %5d %2d %7.1f %7.1f %7.3f %3d %2d %7.2f %7.2f %7.2f %7.2f\n", 
//			ev->id[i], ev->idParent[i],
//		    ev->pdgid[i], ev->charge[i], ev->x[i], ev->y[i], ev->z[i], ev->iz[i], ev->izsub[i], ev->px[i], ev->py[i], ev->pz[i], ev->e2[i]);
		
		nchargedhits++;
		
		
		Particle *p = FindParticle(ev->id[i]);
		if( p==NULL ) {
			p = AddParticle(i);
//			cout << "Added particle" << endl;
		}
		EdbSegP *s = new EdbSegP(i, ev->x[i]*1e3, ev->y[i]*1e3, ev->px[i]/ev->pz[i], ev->py[i]/ev->pz[i], ev->edep[i]*200+16, ev->pdgid[i]);
//			cout << "Track segment z: " << ev->z[i] << endl;
			s->SetZ(ev->z[i]*1e3);
			s->SetPID(ev->iz[i]);
			s->SetPlate(ev->iz[i]);
			s->SetFlag(ev->chamber[i]);
		p->t->AddSegment(s);
		
	}
	printf("ncharged hits %d\n", nchargedhits);
	
	
	printf("%d particles \n", particles->GetEntriesFast());

/*	for(int i=0; i<particles->GetEntriesFast(); i++){
		
		Particle *p = (Particle *) particles->At(i);
		TObjArray parents;
		Particle *pp = p;
		while{pp}{
			printf("%d --> %d", pp->id, pp->idParent);
			pp = FindParticle( pp->idParent);
			if(pp) parents.Add(pp);

		}
		printf("\n%5d parents ", parents.GetEntriesFast());
		for(int j=parents.GetEntriesFast()-1; j>=0; j--){
			Particle *par = (Particle *) parents.At(j);
			printf("%5d --> ", par->id);
		}
		printf("%5d. ", p->id);
		p->Print();
		
	}
	
*/

	TObjArray *primaries = new TObjArray;
	for(int i=0; i<particles->GetEntriesFast(); i++){
		Particle *p = (Particle *) particles->At(i);
		if(p->idParent==0) primaries->Add(p);
	}
	printf("%d primaries\n", primaries->GetEntriesFast());
	
	int depth=1;
	for(int i=0; i<primaries->GetEntriesFast(); i++){
		Particle *p = (Particle *) primaries->At(i);
		int charge = ev->charge[p->index];
		if(charge==0) {
			printf(" id %d, not charged, track follow stopped\n", p->id);
			continue;
		}
		printf(" id %d, charged\n", p->id);
		trackfollow(p, depth+1);
	}
	
	
	delete primaries;
	
}





EdbPVRec *psuedRec(FnuG4 *ev){
	
	EdbPVRec *pvr = new EdbPVRec;
	int nhits = ev->nhits;
	TDatabasePDG db;
	
	for(int i=0; i<nhits; i++){
//		if(i<100) printf("%5d %2d %7.1f %7.1f %7.1f %7.2f %7.2f %7.2f %7.2f\n", 
//		    ev->pdgid[i], ev->charge[i], ev->x[i], ev->y[i], ev->z[i], ev->px[i], ev->py[i], ev->pz[i], ev->e2[i]);
		
		if(ev->charge[i]==0) continue;
		if(ev->izsub[i]==1) continue;
		double slope = sqrt( ev->px[i]*ev->px[i]/ev->pz[i]/ev->pz[i] + ev->py[i]*ev->py[i]/ev->pz[i]/ev->pz[i]);
		if(slope>0.1) continue;

		EdbSegP *s = new EdbSegP(i, ev->x[i]*1e3, ev->y[i]*1e3, ev->px[i]/ev->pz[i], ev->py[i]/ev->pz[i], ev->edep[i]*200+16, ev->pdgid[i]);
		s->SetZ(ev->z[i]*1e3);
		s->SetPID(ev->iz[i]);
		s->SetPlate(ev->iz[i]);
		s->SetP( sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i]+ev->pz[i]*ev->pz[i]));
		ntracks_z->Fill(ev->z[i]);
		cout << "Added track segment with z = " << ev->z[i] << endl;
		//if(sqrt(s->TX()**2+s->TY()**2)>1) continue;
		
		EdbTrackP *t = new EdbTrackP(s);
		
		pvr->AddTrack(t);
		
		
	}
	
	return pvr;
	
}

void display(EdbPVRec *pvr){
	eda = new EdbEDA(pvr,0);
	eda->GetTrackSet("TS")->SetExtendMode(1);
	eda->Run();
}
void display(){
	printf("Open Xin's file\n");
	ev = new FnuG4();

	int iev = 0;
	ev->GetEntry(iev);

	EdbPVRec *pvr = psuedRec(ev);
	display(pvr);
}









void stacking(){
	
	printf("Open Xin's file\n");
	
	ev = new FnuG4();
	
	TFile f("ntracks.root","recreate");
	TNtuple *z = new TNtuple("ntracks_z","","z:iz");
	TH1D *ntrk = new TH1D("ntrk","",100,-700,200);
	TH1D *iz = new TH1D("iz","",100,0,1000);
	TH1D *izfine = new TH1D("iz","",1000,0,1000);
	TH1D *ntrk_pileup = new TH1D("ntrk_pileup","",100,-700,200);
	TH2D *ntrk_iz = new TH2D("ntrk_iz","",50,0,1000,50,10,40000);
	hprof = new TProfile("hprof","Profile of ntracks vs. depth",100,0,1000,0,40000);
	nprof = new TProfile("nprof","Profile of ntracks",100,0,1000,0,40000);
	nmaxprof = new TProfile("nmaxprof","Profile of ntracks",100,0,1000,0,40000);
	izprof = new TProfile("izprof","Profile of ntracks",100,0,1000,0,40000);
	
	TNtuple *nt = new TNtuple("nt","","iev:CC:Enu:x:y:z:ntracks");
	TNtuple *ntReso = new TNtuple("ntReso","","iev:CC:pdg:p:dx0:dy0:dx1:dy1:dx2:dy2:dx3:dy3");
	TNtuple *ntResoSmeared = new TNtuple("ntResoSmeared","","iev:CC:pdg:p:dx0:dy0:dx1:dy1:dx2:dy2:dx3:dy3");
	
	for(int iev=0; iev<1000	; iev++){
		ev->GetEntry(iev);
		int nhits = ev->nhits;
		
		printf("\n\n\n\n [[[[[ iev %d ]]]]] nhits=%d \n", iev, nhits);
		
		ev->Print();
		trackcount=0;
		
		// fake reconstruction.
		// track followdown in this func.
		recTree();
		ntrk->Reset();
//		ntrk_pileup->Reset(); // Commented out to do pileup
		int count = 0;
		int nmax = 0;
		int izmax = 0;
		for(int i=0; i<nhits; i++){
			if(ev->izsub[i]!=0) continue; // There are two hits for 1 emulsion film. Ignore one of them.
			if(ev->pdgid[i]==22) continue; // ignore photons at truth level

			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])>100) continue; // ignore everything outside of 100 um cylinder (optimizing)
//			cout << "z = " << ev->z[i] << endl;
			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])>0.001*abs(ev->z[i]+700.)) continue; // ignore everything outside of 0.001 rad cone (optimizing)
			if(sqrt(ev->x[i]*ev->x[i]+ev->y[i]*ev->y[i])<0.0001*abs(ev->z[i]+700.)) continue; // muon hits are close to axis, ignore them (optimizing)
			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i])/ev->pz[i]>0.05) continue; // Keep only hits with track slope < 50 mrad
			if(sqrt(ev->px[i]*ev->px[i]+ev->py[i]*ev->py[i]+ev->pz[i]*ev->pz[i])<100) continue; // Keep only hits with energy > 100 MeV
			//cout << "x = " << ev->x[i] << " y = " << ev->y[i] << endl;
			z->Fill(ev->z[i],ev->iz[i]);
			ntrk->Fill(ev->z[i]);
			ntrk_pileup->Fill(ev->z[i]);
			iz->Fill(ev->iz[i]);
			count++;
		}
		cout << "count = " << count << endl;
		izmax = ntrk->GetMaximumBin();
		cout << "izmax = " << izmax << endl;
		nmax = ntrk->GetMaximum();
		cout << "nmax = " << nmax << endl;
		nprof->Fill(1,count);
		nmaxprof->Fill(1,nmax);
		izprof->Fill(1,izmax);
		for(int i=0;i<100;i++){
			double zVal = ntrk->GetXaxis()->GetBinCenter(i);
			double izVal = iz->GetXaxis()->GetBinCenter(i);
			int nVal = ntrk->GetBinContent(i);
//			cout << "center = " << zVal << endl;
//			cout << "bin content = " << nVal << endl;
//			ntrk_z->Fill(zVal,nVal);
			ntrk_iz->Fill(izVal,nVal);
			hprof->Fill(izVal,nVal);
		}
	}
	hprof->Write();
	nprof->Write();
	nmaxprof->Write();
	izprof->Write();
	ntrk->Write();
	iz->Write();
	ntrk_pileup->Write();
	ntrk_iz->Write();
	ntracks_z->Write();
	nt->Write();
	f.Close();
	TFile *file = new TFile("ntracks.root");
	TCanvas *c = new TCanvas();
	TH1D *h = (TH1D*)file->Get("hprof");
	h->SetMarkerStyle(8);
	h->SetMarkerSize(0.5);
	c->SetLogy();
	h->Draw();
//     EdbPVRec *pvr = psuedRec(ev); 	display(pvr);

	
}

